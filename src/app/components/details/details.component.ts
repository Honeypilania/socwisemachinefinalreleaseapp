import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MachineService } from '../../services/machine/machine.service';
import { DataSource } from "@angular/cdk/collections";
import { VehicleService } from '../../services/vehicle/vehicle.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})


export class DetailsComponent implements OnInit , OnDestroy 
{

 isMachine:boolean;
 isInsurance:boolean;
 isPermit:boolean;
 isRoadTax:boolean;

 data;

 $today = new Date();
 $yesterday = new Date(this.$today);

 date1;
 date2;
 date3;
 date4;
 date5;
 date6;
 date7;

 date_diff_indays = function(date1, date2) {
  var dt1 = new Date(date1);
 var  dt2 = new Date(date2);
  return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
  }

 isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

    displayedColumns: string[] = ['machinName','totalHours','dailyHour','dailyHour2','dailyHour3','dailyHour4','dailyHour5','dailyHour6','dailyHour7'];
    dataSource = new MachineData(this.machine);

    displayedColumns2: string[] = ['machinName','insurance','issue','expire','status'];
    dataSource2 = new MachineData(this.machine);

    displayedColumns3: string[] = ['vehicleName','insurance','issue','expire','status'];
    dataSource3 = new VehicleData(this.vehicle);

  constructor(private route:ActivatedRoute,private machine:MachineService,private vehicle:VehicleService,private breakpointObserver: BreakpointObserver) {
    this.date55();
   }

  ngOnInit() 
  {
    this.route.paramMap.subscribe(arr=>
                                        {
                                          if (arr.get('id')=='machine') this.isMachine=true;
                                          else if(arr.get('id')=='insurance') this.isInsurance=true;
                                          else if(arr.get('id')=='permit') this.isPermit=true;
                                          else if(arr.get('id')=='roadtax') this.isRoadTax=true;
                                        }
                                  );
  }

  ngOnDestroy()
  {
      this.isMachine=this.isInsurance=this.isRoadTax=this.isPermit=false;
  }

  giveDate(d)
  {
    var value;
    value=parseInt(d)
    return (new Date(value).toDateString())
  }
  valid(d)
  {
    if(this.date_diff_indays(new Date(),new Date(d))<=30)
      return true;
      else
        return false;
  }
  date(s)
  {
     switch (s) {
       case 1:
         return this.date1;
       case 2:
         return this.date2;
       case 3:
         return this.date3;
       case 4:
         return this.date4;
       case 5:
         return this.date5;
       case 6:
         return this.date6;
       case 7:
         return this.date7;
       default:
            break;
     }
  }

  hour(i,j)
  {
    this.data=this.machine.machineList;
    var x=this.data[i].dailyHour;
    switch (j) {
      case 1:
                for(let element of x)
                {
                  var d=(new Date(element.date.seconds*1000)).toDateString();
                  if(d==this.date1.toDateString())
                  {
                    return element.hours+'';
                  }
                }
                return '-';
      case 2:
                for(let element of x)
                {
                  var d=(new Date(element.date.seconds*1000)).toDateString();
                  if(d==this.date2.toDateString())
                  {
                    return element.hours+'';
                  }
                }
                return '-';
      case 3:
                for(let element of x)
                {
                  var d=(new Date(element.date.seconds*1000)).toDateString();
                  if(d==this.date3.toDateString())
                  {
                    return element.hours+'';
                  }
                }
                return '-';
      case 4:
                for(let element of x)
                {
                  var d=(new Date(element.date.seconds*1000)).toDateString();
                  if(d==this.date4.toDateString())
                  {
                    return element.hours+'';
                  }
                }
                return '-';
      case 5:
                for(let element of x)
                {
                  var d=(new Date(element.date.seconds*1000)).toDateString();
                  if(d==this.date5.toDateString())
                  {
                    return element.hours+'';
                  }
                }
                return '-';
      case 6:
                for(let element of x)
                {
                  var d=(new Date(element.date.seconds*1000)).toDateString();
                  if(d==this.date6.toDateString())
                  {
                    return element.hours+'';
                  }
                }
                return '-';
      case 7:
                for(let element of x)
                {
                  var d=(new Date(element.date.seconds*1000)).toDateString();
                  if(d==this.date7.toDateString())
                  {
                    return element.hours+'';
                  }
                }
                return '-';
      default:
        break;
    }
  }

  date55()
  {
    this.date1=(new Date(this.$yesterday.setDate(this.$today.getDate())));
    this.date2=(new Date(this.$yesterday.setDate(this.date1.getDate() - 1)));
    this.date3=(new Date(this.$yesterday.setDate(this.date2.getDate() - 1)));
    this.date4=(new Date(this.$yesterday.setDate(this.date3.getDate() - 1)));
    this.date5=(new Date(this.$yesterday.setDate(this.date4.getDate() - 1)));
    this.date6=(new Date(this.$yesterday.setDate(this.date5.getDate() - 1)));
    this.date7=(new Date(this.$yesterday.setDate(this.date6.getDate() - 1)));
  }
  search(){
  alert("ooooooooooooooooooooooooooo yehhhhhhhhhhhhhhhhhhhhhh");
  }

}


export class MachineData extends DataSource<any>
{
  constructor(private machine:MachineService){super()}
  connect()
  {
    return this.machine.getValue();
  }
  disconnect()
  {

  }
}

export class VehicleData extends DataSource<any>
{
  constructor(private vehicle:VehicleService){super()}
  connect()
  {
    return this.vehicle.getValue();
  }
  disconnect()
  {

  }
 
}

