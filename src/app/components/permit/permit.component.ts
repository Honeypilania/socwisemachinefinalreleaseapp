import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { DataSource } from '@angular/cdk/table';
import { FormControl } from '@angular/forms';
import { VehicleService } from '../../services/vehicle/vehicle.service';
import { MatDialog } from '@angular/material';
import { DialogComponent } from '../dialog/dialog.component';
import { NotificationsService } from 'angular2-notifications';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-permit',
  templateUrl: './permit.component.html',
  styleUrls: ['./permit.component.scss']
})
export class PermitComponent implements OnInit, OnDestroy {
  enable=[];
  data;
  data1;
  data2;
  data3;
  data4;
  isPending:boolean;
  isDetails:boolean;
  isEntry:boolean;
  p;
  admin;
  constructor(private email:AngularFireAuth,private router:ActivatedRoute,private afs:AngularFirestore,public vehicle:VehicleService,private dialog:MatDialog,private msg:NotificationsService) 
  {

    afs.collection('user').valueChanges().subscribe(usr=>{
      for(let user of usr)
      {
        this.p=user;
        if(this.p.email==this.email.auth.currentUser.email)
        {
          if(this.p.admin)
          {
            this.admin=true;
          }
        }
      }
    });
    this.afs.collection('vehicle').valueChanges().subscribe(arr=>{
      for(let i=0;i<arr.length;i++) this.enable[i]=false;
    })
   }

   ngOnInit() 
  {
    this.router.paramMap.subscribe(arr=>
                                        {
                                          if (arr.get('id')=='pending') this.isPending=true;
                                          else if(arr.get('id')=='details') this.isDetails=true;
                                          else if(arr.get('id')=='entry') this.isEntry=true;
                                        }
                                  );
  }

  ngOnDestroy()
  {
      this.isDetails=this.isEntry=this.isPending=false;
  }

  displayedColumns: string[] = ['permit','amount', 'issue', 'expire', 'status'];
  dataSource = new VehicleData(this.afs);
  date2=new FormControl();
  add=false;

  date_diff_indays = function(date1, date2) {
    var dt1 = new Date(date1);
   var  dt2 = new Date(date2);
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }

  convert_in_date = function(str){
    var d1d=str.split('/')
    var date=parseInt(d1d[0])
    var month=parseInt(d1d[1]) 
    var year=parseInt(d1d[2])
    var newDate=new Date(year,month-1,date)
    return newDate;
  }


  date(d)
  {
    return (new Date(parseInt(d)*1000)).toLocaleDateString()
  }
create()
{
  this.afs.collection('UP 12 B1534').add({
    permit:'NP',
    amount:'12000',
    issueDate:(new Date()),
    expireDate:(new Date()),
    $key:''
  }).then(arr=>{
    arr.update({'$key':arr.id});
    alert('Done')
  }).catch(err=>{
    alert(err)
  })
}

check(i,vehicle)
{
  this.data1=document.getElementById('permit'+i)
  this.data2=document.getElementById('amount'+i)
  this.data3=document.getElementById('from'+i)
  this.data4=document.getElementById('till'+i)
  var date1=this.convert_in_date(this.data3.value)
  var date2=this.convert_in_date(this.data4.value)
  if(this.data1.value=='' || this.data2.value=='' || this.data2.value=='' || this.data2.value=='' )
  {
    this.msg.error('All fields are required');
  }
  else
  {
    if(date1.getTime()>date2.getTime() || date2.getTime()<(new Date()).getTime()){
      this.msg.error('Invalid Dates..');
    }
    else{
      document.getElementById('cvp').click();
      this.msg.success('all done')
      this.vehicle.addPermit(this.data1.value,this.data2.value,this.convert_in_date(this.data3.value),this.convert_in_date(this.data4.value),vehicle)
    }
  }
  
}

remove(a,b,c,d,e)
{
  this.dialog
  .open(DialogComponent)
  .afterClosed()
  .subscribe(result=>{
                        if(result=='confirm') 
                        {
                          this.vehicle.removePermit(a,b,c,d,e);
                        }
                        else {

                        };
                      })
}

giveDate(d)
  {
    var value;
    value=parseInt(d)
    return (new Date(value).toDateString())
  }

  valid(d)
  {
    if(this.date_diff_indays(new Date(),new Date(d))<=30)
      return true;
      else
        return false;
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  alphaOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 65 || charCode > 90 )) {
      this.msg.error('Capital Letters Only');
      return false;
    }
    return true;

  }
}


export class VehicleData extends DataSource<any>
{
  constructor(private afs:AngularFirestore){super()}
  connect()
  {
    return this.afs.collection('UP 12 B1534').valueChanges();
  }
  disconnect()
  {

  }
}