import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataSource } from '@angular/cdk/table';
import { MachineService } from '../../services/machine/machine.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { NotificationsService } from 'angular2-notifications';
import { AngularFireAuth } from '@angular/fire/auth';
import { VehicleService } from '../../services/vehicle/vehicle.service';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-entries',
  templateUrl: './entries.component.html',
  styleUrls: ['./entries.component.scss']
})
export class EntriesComponent implements OnInit, OnDestroy 
{
  myDates1
  myData 
  myDates2
  enable=[];
  enable2=[];
  errorIn=[];
  error1=false;
  last=true;
  edit=false;
  today=new Date();
  date;
  date1;
  date2=new FormControl();
  d1;
  d3;
  butS=false;
  displayedColumns: string[] = ['machinName','last', 'input', '250', '1K','6K'];
  dataSource = new MachineData(this.machine);

  displayedColumns2: string[] = ['machinName','insurance','issue','expire','status'];
  dataSource2 = new MachineData(this.machine);

  displayedColumns3: string[] = ['vehicleName','insurance','issue','expire','status'];
  dataSource3 = new VehicleData(this.vehicle);


  isMachine:boolean;
  isInsurance:boolean;
  isPermit:boolean;
  isRoadTax:boolean;
  d2;
  d4;
  d5;
  diff=true;
  diffNum;
  diffNum2;
  admin=false;
  p;
  
  date_diff_indays = function(date1, date2) {
    var dt1 = new Date(date1);
   var  dt2 = new Date(date2);
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }

  convert_in_date = function(str){
    var d1d=str.split('/')
    var date=parseInt(d1d[0])
    var month=parseInt(d1d[1]) 
    var year=parseInt(d1d[2])
    var newDate=new Date(year,month-1,date)
    return newDate;
  }
  constructor(private route:ActivatedRoute,private machine:MachineService,afs:AngularFirestore,private notify:NotificationsService,private email:AngularFireAuth,private vehicle:VehicleService) 
  {
    this.d4=machine.machineList;
    afs.collection('user').valueChanges().subscribe(usr=>{
      for(let user of usr)
      {
        this.p=user;
        if(this.p.email==this.email.auth.currentUser.email)
        {
          if(this.p.admin)
          {
            this.admin=true;
          }
        }
      }
    });

    
    for(let i=0;i<machine.machineList.length;i++)
    {
      this.enable[i]=false;
    };

    for(let i=0;i<vehicle.vehicleList.length;i++)
    {
      this.enable2[i]=false;
    }
  }



  hide()
  {
    if(this.displayedColumns.length==6)
     this.displayedColumns=['machinName','last', '250', '1K','6K'];
    else
     this.displayedColumns= ['machinName','last', 'input', '250', '1K','6K'];
  }

  checkValuesFirst()
  {
    this.error1=false;
    var count=0;
    this.d4=this.machine.machineList;
    var e=this.machine.machineList.length;
    for(let i=0;i<e ;i++)
    {
       this.d3=document.getElementById(i+'');
       if(this.d3.value=='')
       {
          count++
       }
       else
       {
          if((new Date(this.d4[i].dailyHour[this.d4[i].dailyHour.length-1].date.seconds*1000)).toLocaleDateString()==(new Date()).toLocaleDateString())
          {
           if(parseInt(this.d3.value)<parseInt(this.d4[i].dailyHour[this.d4[i].dailyHour.length-2].hours) || (parseInt(this.d3.value)-parseInt(this.d4[i].dailyHour[this.d4[i].dailyHour.length-2].hours))>(this.date_diff_indays((new Date(this.d4[i].dailyHour[this.d4[i].dailyHour.length-2].date.seconds*1000)),(new Date()))*20))
            {
              this.notify.error('Invalid Value','value '+(i+1)+' is invalid');
              this.d3.focus();
              break;
            }
            else count++;
          }
          else
          {
              if (parseInt(this.d3.value)<parseInt(this.d4[i].dailyHour[this.d4[i].dailyHour.length-1].hours) || (parseInt(this.d3.value)-parseInt(this.d4[i].dailyHour[this.d4[i].dailyHour.length-1].hours))>(this.date_diff_indays((new Date(this.d4[i].dailyHour[this.d4[i].dailyHour.length-1].date.seconds*1000)),(new Date()))*20)) 
              {
                this.notify.error('Invalid Value','value '+(i+1)+' is invalid');
                this.d3.focus();
                break;
              } 
              else count++;
          }
       }
       if(count==e)
       this.data();
    }
  }

  data()
  {
    this.d4=this.machine.machineList;
    var c=this.machine.machineList.length;
    var count=0
    for(let i=0;i<c;i++)
    {
      this.d3=document.getElementById(i+'');
      if(this.d3.value==''){}
      else if(this.date_diff_indays((new Date(this.d4[i].dailyHour[this.d4[i].dailyHour.length-1].date.seconds*1000)),(new Date()))>7)
      {
          this.notify.error('Value '+(i+1)+' Blocked');
          this.error1=true;
          this.errorIn[count]=this.d4[i].machinName;
          count++;
          this.d3.value='';
      }
       else 
       {
        this.d5=document.getElementById(i+'last');
        var freshDiff=parseInt(this.d3.value)-parseInt(this.d5.value);
        var OldDiff=parseInt(this.d3.value)-parseInt(this.d4[i].dailyHour[this.d4[i].dailyHour.length-1].hours);
        if((new Date(this.d4[i].dailyHour[this.d4[i].dailyHour.length-1].date.seconds*1000)).toLocaleDateString()==(new Date()).toLocaleDateString())
        {
          this.machine.updateTwoC(i,OldDiff);
          this.machine.updateOneC(i,OldDiff);
          this.machine.updateSixC(i,OldDiff);
          this.machine.updateArray(i,this.d3.value,true);
          this.machine.updateTotal(i,this.d3.value);
        }
        else
        {
          this.machine.updateArray(i,this.d3.value,false);
          this.machine.updateTotal(i,this.d3.value);
          this.machine.updateTwoC(i,freshDiff);
          this.machine.updateOneC(i,freshDiff);
          this.machine.updateSixC(i,freshDiff);
        }
       }
    }
    setTimeout(() => {
      this.notify.success('Submitted');
    }, 2000);
  }



  ngOnInit() 
  {
    this.route.paramMap.subscribe(arr=>
                                        {
                                          if (arr.get('id')=='hours') this.isMachine=true;
                                          else if(arr.get('id')=='insurance') this.isInsurance=true;
                                          else if(arr.get('id')=='permit') this.isPermit=true;
                                          else if(arr.get('id')=='roadtax') this.isRoadTax=true;
                                        }
                                  );
  }

  ngOnDestroy()
  {
      this.isMachine=this.isInsurance=this.isRoadTax=this.isPermit=false;
  }



  val(i)
  {
    var date=(new Date()).toDateString();
    this.d2=this.machine.machineList;
    var d=this.d2[i].dailyHour;
    for(let element of d)
    {
      var d1=(new Date(element.date.seconds*1000)).toDateString();
      if(d1==date)
      {
        return element.hours;
      }
    }
    return '';
  }



  val1(i)
  {
    var d;
    this.d2=this.machine.machineList;
    if((new Date(this.d2[i].dailyHour[this.d2[i].dailyHour.length-1].date.seconds*1000)).toLocaleDateString()==(new Date()).toLocaleDateString())
    d=this.d2[i].dailyHour[this.d2[i].dailyHour.length-2].hours;
    else d=this.d2[i].dailyHour[this.d2[i].dailyHour.length-1].hours;
    if(d) return d;
    else return '';
  }

  date11(i)
  {
    var d;
    this.d2=this.machine.machineList;
    if((new Date(this.d2[i].dailyHour[this.d2[i].dailyHour.length-1].date.seconds*1000)).toLocaleDateString()==(new Date()).toLocaleDateString())
      d=(new Date(this.d2[i].dailyHour[this.d2[i].dailyHour.length-2].date.seconds*1000)).toLocaleDateString();
    else d=(new Date(this.d2[i].dailyHour[this.d2[i].dailyHour.length-1].date.seconds*1000)).toLocaleDateString();
    if(d) return d;
    else return '';
  }



 twoFifty(key,i)
 {
   this.machine.twoFifty(key,i);
 }



 oneK(key,i)
 {
   this.machine.oneK(key,i);
 }



 sixK(key,i)
 {
   this.machine.sixK(key,i);
 }



  check2(i)
  {
    this.d1=this.machine.machineList;
    var status=parseInt(this.d1[i].twoFifty.counter);
    var check=parseInt(this.d1[i].twoFifty.check)
    if(status<check) return true;
    else 
      {
        return false;
      }
  }



  check1(i)
  {
    this.d1=this.machine.machineList;
    var status=parseInt(this.d1[i].oneK.counter);
    var check=parseInt(this.d1[i].oneK.check);
    if(status<check) return true;
    else 
      {
        return false;
      }
  }


  check6(i)
  {
    this.d1=this.machine.machineList
    var status=parseInt(this.d1[i].sixK.counter);
    var check=parseInt(this.d1[i].sixK.check);
    if(status<check) return true;
    else 
      {
        return false;
      }
  }


  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  alphaOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 65 || charCode > 90 )) {
      this.notify.error('Capital Letters Only');
      return false;
    }
    return true;

  }


  butD()
  {
    this.butS=true;
  }


  butE()
  {
    this.butS=false;
  }


  checkValues()
  {
    var count=0;
    this.d4=this.machine.machineList;
    var e=this.machine.machineList.length;
    
    for(let i=0;i<e ;i++)
    {
       this.d5=document.getElementById(i+'last');
       if(this.d5.value=='')
       {
          this.notify.error('Blank Value','input '+(i+1)+' has no value');
          this.d5.focus();
          break;
       }
       else
       {
          var d=(new Date(this.d4[i].dailyHour[this.d4[i].dailyHour.length-1].date.seconds*1000)).toLocaleDateString();
          if(d==(new Date()).toLocaleDateString())
          {
            if(parseInt(this.d5.value)<parseInt(this.d4[i].dailyHour[this.d4[i].dailyHour.length-3].hours) || (parseInt(this.d5.value)-parseInt(this.d4[i].dailyHour[this.d4[i].dailyHour.length-3].hours))>(this.date_diff_indays((new Date(this.d4[i].dailyHour[this.d4[i].dailyHour.length-3].date.seconds*1000)),(new Date(this.d4[i].dailyHour[this.d4[i].dailyHour.length-2].date.seconds*1000)))*20) || (parseInt(this.d4[i].dailyHour[this.d4[i].dailyHour.length-1].hours)-parseInt(this.d5.value))>(this.date_diff_indays((new Date(this.d4[i].dailyHour[this.d4[i].dailyHour.length-2].date.seconds*1000)),(new Date(this.d4[i].dailyHour[this.d4[i].dailyHour.length-1].date.seconds*1000)))*20))
            {
              this.notify.error('Invalid Value','value '+(i+1)+' is invalid');
              this.d5.focus();
              break;
            }
            else count++;
          }
          else
          {
              if (parseInt(this.d5.value)<parseInt(this.d4[i].dailyHour[this.d4[i].dailyHour.length-2].hours) || (parseInt(this.d5.value)-parseInt(this.d4[i].dailyHour[this.d4[i].dailyHour.length-2].hours))>(this.date_diff_indays((new Date(this.d4[i].dailyHour[this.d4[i].dailyHour.length-2].date.seconds*1000)),(new Date(this.d4[i].dailyHour[this.d4[i].dailyHour.length-1].date.seconds*1000)))*20)) 
              {
                this.notify.error('Invalid Value','value '+(i+1)+' is invalid');
                this.d5.focus();
                break;
              } 
              else count++;
          }
       }
       if(count==e)
       this.update();
    }
  }


  update()
  {  
    this.d4=this.machine.machineList;
    var e=this.machine.machineList.length;
      for(let i=0;i<e ;i++)
      {
        var d=(new Date(this.d4[i].dailyHour[this.d4[i].dailyHour.length-1].date.seconds*1000)).toLocaleDateString();
        this.d5=document.getElementById(i+'last')
        if(d==(new Date()).toLocaleDateString())
        this.machine.lastEntered(i,this.d5.value);
        else
        this.machine.lastFresh(i,this.d5.value); 
      }
    }

  //   giveDate(d)
  // {
  //   var value;
  //   value=parseInt(d)
  //   return this.date2 = new FormControl(new Date(value));
  // }

  giveDate2(d)
  {
    var value;
    value=parseInt(d)
    return (new Date(value)).toDateString();
  }

  valid(d)
  {
    if(this.date_diff_indays(new Date(),new Date(d))<=30)
      return true;
      else
        return false;
  }
   
  checkData(data,i:boolean,key,check:string)
  {
   var error=true;
   var date1:Date
   var date2:Date
   this.myDates1=document.getElementById(data+'from');
   this.myDates2=document.getElementById(data+'till');
   this.myData=document.getElementById(data+'type');
   if(i){
    if (this.myData.value=='' || this.myDates1.value=='' || this.myDates2.value=='') 
    {
      this.notify.error('All Fields Are Required');
      error=false;
      return; 
    }
    else
    {
      var today=new Date();
      date1=this.convert_in_date(this.myDates1.value);
      date2=this.convert_in_date(this.myDates2.value);
     if(this.date_diff_indays(date1,date2)<0 || this.date_diff_indays(today,date2)<0)
     {
       this.notify.error('Invalid Dates');
       error=false;
       return;
     }
    } 
   }
   else {
    if (this.myData.value=='' || this.myDates1.value=='' || this.myDates2.value=='') 
    {
      this.notify.error('All Fields Are Required');
      error=false;
      return; 
    }
    else
    {
      var today=new Date();
     var date1=this.convert_in_date(this.myDates1.value);
     var date2=this.convert_in_date(this.myDates2.value);
     if(date1==undefined || date2==undefined)
     {
      this.notify.error('Invalid Dates');
      error=false;
      return;
     }
     if(this.date_diff_indays(date1,date2)<0 || this.date_diff_indays(today,date2)<0)
     {
       this.notify.error('Invalid Dates');
       error=false;
       return;
     }
    } 
   }
   if(error)
   {
     switch (check) {
          case 'mi' :
                     this.machine.updateInsurance(key,date1,date2,this.myData.value);
          break;
          case 'vi' :
                     this.vehicle.updateInsurance(key,date1,date2,this.myData.value);
          break;

          case 'vp' :
                      this.vehicle.updatePermit(key,date1,date2,this.myData.value);
          break;

          default:
          break;
     }
   }
  }

}


export class MachineData extends DataSource<any>
{
  constructor(private machine:MachineService){super()}
  connect()
  {
    return this.machine.getValue();
  }
  disconnect()
  {

  }
}

export class VehicleData extends DataSource<any>
{
  constructor(private vehicle:VehicleService){super()}
  connect()
  {
    return this.vehicle.getValue();
  }
  disconnect()
  {

  }
}
