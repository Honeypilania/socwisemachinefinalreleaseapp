import { Component,OnInit} from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { MachineService } from '../../services/machine/machine.service';
import { VehicleService } from '../../services/vehicle/vehicle.service';
import { AngularFireDatabase } from '@angular/fire/database';
@Component({
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.scss']
})
export class DashComponent 
{
  value;
  date=true;
  mp;
  ip=0;
  pp=0;
  data;
  date_diff_indays = function(date1, date2) {
    var dt1 = new Date(date1);
   var  dt2 = new Date(date2);
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }
  pp1=this.afs.collection('vehicle').valueChanges().subscribe(arr=>{
    for(let element of arr)
    {
      this.data=element;
      for(let data of this.data.permit)
      {
        if(this.valid(data.expireDate.seconds*1000))
        {
          this.pp++;
          console.log(this.pp)
        }
      }
    }
  })
  ip1=this.afs.collection('vehicle',ref=>ref.where('insrncStatus','==',false)).valueChanges().subscribe(arr=>{
   this.ip=this.ip+arr.length;
    this.vehicle.insuranceCheck();
  });
  ip2=this.afs.collection('machine',ref=>ref.where('insrncStatus','==',false)).valueChanges().subscribe(arr=>{
    this.ip=this.ip+arr.length;
    this.machine.insuranceCheck();
  });
  mac=this.afs.collection('machine',ref=>ref.where('allSerStatus','==',false))
  .valueChanges()
  .subscribe(arr=>{
    if (arr.length==0) this.mp='';
     else this.mp=arr.length;
  });

  card;
   cards= this.breakpointObserver.observe(Breakpoints.Handset).pipe(
     map(({ matches }) => {
       if (matches) {
         return [
           { title: 'Card 1', cols: 4, rows: 1 },
           { title: 'Card 2', cols: 4, rows: 1 },
           { title: 'Card 3', cols: 4, rows: 1 },
           { title: 'Card 4', cols: 4, rows: 1 },
           { title: 'Card 5', cols: 4, rows: 1 },
         ];
       }
 
       return [
           { title: 'Card 1', cols: 1, rows: 1 },
           { title: 'Card 2', cols: 1, rows: 1 },
           { title: 'Card 3', cols: 1, rows: 1 },
           { title: 'Card 4', cols: 1, rows: 1 },
           { title: 'Card 5', cols: 1, rows: 1 },
       ];
     })
   ).subscribe(arr=>this.card=arr);
   isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
   .pipe(
     map(result => result.matches)
   );
   constructor(private db:AngularFireDatabase,private breakpointObserver: BreakpointObserver,private route:Router,private afs:AngularFirestore,private vehicle:VehicleService,private machine:MachineService) 
   {
      
   }
   
   ngOnInit()
   {
    this.db.object('Date').valueChanges().subscribe(arr=>{
      this.value=arr;
      if((new Date(parseInt(this.value))).toLocaleDateString()!=(new Date()).toLocaleDateString())
      {
        this.date=false;
      }
   })
  }

  details(str)
   {
     this.route.navigateByUrl('home/details/'+str);
   }
   pending(str)
   {
     this.route.navigateByUrl('home/pending/'+str);
   }
   entries(str)
   {
    this.route.navigateByUrl('home/entry/'+str);
   }

   valid(d)
   {
     if(this.date_diff_indays(new Date(),new Date(d))<=30)
       return true;
       else
         return false;
   }
 }
 
