import { Component } from '@angular/core';
import { FormGroup,FormControl ,Validators} from "@angular/forms";
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { UserService } from '../services/users/user.service';
import { AngularFireDatabase } from '@angular/fire/database';
import * as firebase from 'firebase'
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  hide=true;
  form=new FormGroup({
    email:new FormControl('', [Validators.required, Validators.email,Validators.minLength(10),Validators.maxLength(50)]),
    password:new FormControl('', [Validators.required,Validators.maxLength(20)]),
});
date;

    model : any={};
    constructor(db:AngularFireDatabase,private router:Router,private msg:NotificationsService,private user:UserService) 
    {  
      db.object('Date').set(firebase.database.ServerValue.TIMESTAMP);
    }

     getEmailError() 
  {
      return this.form.get('email').hasError('required') ? 'Enter Email' :
              this.form.get('email').hasError('email') ? 'Enter valid email' :
                this.form.get('email').hasError('minlength') ? 'Minimum 10 Character' : '';
  }
  getPassError() 
  {
      return this.form.get('password').hasError('required') ? 'Enter Password' : '';
  }
  login()
  {
     
    this.user.login(this.model.email,this.model.password);

    }
   /*this.date=this.vehicle.vehicleList;
    var d=new Date(this.date[0].insurance[0].expireDate.seconds*1000)
    console.log('Date:'+d.toLocaleDateString(),'Time:'+d.toLocaleTimeString());*/
  }


