import { Timestamp } from "rxjs";

export class Machine{
    $key: string;
    dailyHour: Array<Map<Timestamp<Date>,string>>;
    installDate: Timestamp<Date>;
    insurance: Array<Map<Timestamp<Date>,string>>;
    machineName: string;
    oneK: Map<string,string>;
    sixK: Map<string,string>;
    twoFifty: Map<string,string>;
    totalHours: string;
}