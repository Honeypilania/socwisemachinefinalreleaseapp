import { Timestamp } from "rxjs";

export class Vehicle{
    $key: string;
    permit:string;
    vehicleName: string;
    vehicleNumber: string;
    insurance: Array<Map<Timestamp<Date>,string>>;
    roadTax: Array<Map<Timestamp<Date>,string>>;
}