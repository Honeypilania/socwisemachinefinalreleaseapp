import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ErrorComponent } from './components/error/error.component';
import { DashComponent } from './components/dash/dash.component';
import { DetailsComponent } from './components/details/details.component';
import { PendingComponent } from './components/pending/pending.component';
import { EntriesComponent } from './components/entries/entries.component';
import { AuthGuard } from './services/auth-guard/authguard.service';
import { PermitComponent } from './components/permit/permit.component';
const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'dash', component: DashboardComponent,canActivate:[AuthGuard], children: [ 
            { path: '', component: DashComponent},
            {path:'details/:id',component:DetailsComponent},
            {path:'pending/:id',component:PendingComponent},
            {path:'entry/:id',component:EntriesComponent},
            {path: 'permit/:id', component:PermitComponent}
  ]},
{ path: '', redirectTo: '/home', pathMatch: 'full' }, 
{ path: '**',redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
