import { Injectable } from '@angular/core';
import { Vehicle } from '../../models/vehicle.model';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { NotificationsService } from 'angular2-notifications';
import * as firebase from "firebase"
@Injectable({
  providedIn: 'root'
})
export class VehicleService 
{

  editMode: boolean;
  vehicleList: Vehicle[];
  vehicleCollection: AngularFirestoreCollection<Vehicle>;
  vehicleListView = true;
  data;
  date_diff_indays = function(date1, date2) {
    var dt1 = new Date(date1);
   var  dt2 = new Date(date2);
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }
  constructor(private afs:AngularFirestore,private msg:NotificationsService) {
    this.editMode = false;
    this.vehicleCollection = this.afs.collection('vehicle');


      this.vehicleListView = true;
       const x = this.getData();
       x.snapshotChanges().subscribe(item => {
          this.vehicleList = [];
          item.forEach(element => {
          const data = element.payload.doc.data() as Vehicle;
          data.$key = element.payload.doc.id;
          this.vehicleList.push(data);
        });
       });
    }

    getData() {
      this.vehicleCollection = this.afs.collection('vehicle');
      return this.vehicleCollection;
    }
      getValue() 
    {
      
      return this.afs.collection('vehicle').valueChanges();
    }

    insuranceCheck()
    {
      this.data=this.vehicleList;
      for(let i=0;i<this.data.length;i++)
      {
        if(this.date_diff_indays(new Date(),new Date(this.data[i].insurance[this.data[i].insurance.length-1].expireDate.seconds*1000))<=30)
      {
        this.vehicleCollection.doc(this.data[i].$key).update({'insrncStatus':false});
      }
        else this.vehicleCollection.doc(this.data[i].$key).update({'insrncStatus':true});
      }
    }

    permitCheck()
    {
      this.data=this.vehicleList;
      for(let i=0;i<this.data.length;i++)
      {
        if((this.data[i].permit[this.data[i].permit.length-1].expireDate.seconds*1000)<(new Date()).getTime())
      {
        this.vehicleCollection.doc(this.data[i].$key).update({'permitStatus':false});
      }
       else  this.vehicleCollection.doc(this.data[i].$key).update({'permitStatus':true});
      }
    }

    updateInsurance(key,issue:Date,expire:Date,amount)
    {
      this.vehicleCollection.doc(key).update({insurance:firebase.firestore.FieldValue.arrayUnion({amount:amount,issueDate:issue,expireDate:expire})}).then(()=>{
        this.vehicleCollection.doc(key).update({'insrncStatus':true});
      }).then(()=>
      {
        this.msg.success('Done');
        document.getElementById('cancel').click();
      }).catch(()=>{
        this.msg.error('Connection Problem')
      });
    }

    updatePermit(key,issue:Date,expire:Date,type)
    {
      this.vehicleCollection.doc(key).update({permit:firebase.firestore.FieldValue.arrayUnion({type:type,issueDate:issue,expireDate:expire})}).then(()=>{
        this.vehicleCollection.doc(key).update({'permitStatus':true});
      }).then(()=>
    {
      this.msg.success('Done');
      document.getElementById('cancel').click();
    }).catch(()=>{
      this.msg.error('Connection Problem')
    });
    }
    
    removePermit(type,amount,issue,expire,key)
    {
      this.vehicleCollection.doc(key).update({permit:firebase.firestore.FieldValue.arrayRemove({type:type,amount:amount,issueDate:issue,expireDate:expire})}).then(()=>{
        this.msg.success("Value Deleted");
      }).catch(err=>{
        this.msg.error(err);
      })
    }

    addPermit(type,amount,issue,expire,key)
    {
      this.vehicleCollection.doc(key).update({permit:firebase.firestore.FieldValue.arrayUnion({type:type,amount:amount,issueDate:issue,expireDate:expire})}).then(()=>{
        this.msg.success("Value Submitted");
      }).catch(err=>{
        this.msg.error(err);
      })
    }
    
}
