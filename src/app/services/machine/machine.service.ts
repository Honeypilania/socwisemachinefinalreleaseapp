import { Injectable } from '@angular/core';
import { AngularFirestore,AngularFirestoreCollection } from "@angular/fire/firestore";
import { Machine } from "../../models/machine.model";
import * as firebase from "firebase"
import { NotificationsService } from 'angular2-notifications';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { AlertController, Platform } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class MachineService 
{
  data222
  editMode: boolean;
  machineList: Machine[];
  machineCollection: AngularFirestoreCollection<Machine>;
  machineListView = true;
  data;
  found=false;
  index;
  test;
  test2;
  data2;
  date_diff_indays = function(date1, date2) {
    var dt1 = new Date(date1);
   var  dt2 = new Date(date2);
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }
  constructor(private afs: AngularFirestore, public platform: Platform,
    public alertCtrl: AlertController, private localNotifications: LocalNotifications,private msg:NotificationsService) 
  {
    this.editMode = false;
    this.machineCollection = this.afs.collection('machine');


      this.machineListView = true;
       const x = this.getData();
       x.snapshotChanges().subscribe(item => {
          this.machineList = [];
          item.forEach(element => {
          const data = element.payload.doc.data() as Machine;
          data.$key = element.payload.doc.id;
          this.machineList.push(data);
        });
       });
  }


    getData()
    {
      this.machineCollection = this.afs.collection('machine');
      return this.machineCollection;
    }

    getValue() 
    {
      
      return this.afs.collection('machine').valueChanges();
    }


   
    twoFifty(key,i)
    {
      this.data=this.machineList;
      var previous=this.data[i].twoFifty.counter
      var d:boolean=true;
      this.machineCollection.doc(key).update({'twoFifty.lastStatus':previous}).then(()=>{
        this.machineCollection.doc(key).update({'twoFifty.status':d});
        this.machineCollection.doc(key).update({'twoFifty.counter':'0'});
      }).then(()=>{
        if (this.data[i].oneK.status==true && this.data[i].sixK.status==true) 
        {
          this.machineCollection.doc(key).update({'allSerStatus':true});
        } 
      }).catch(err=>{this.msg.error('Connection Problem')});
      
    }

    oneK(key,i)
    {
      this.data=this.machineList;
      var previous=this.data[i].oneK.counter
      var d:boolean=true;
      this.machineCollection.doc(key).update({'oneK.lastStatus':previous}).then(()=>{
        this.machineCollection.doc(key).update({'oneK.status':d});
        this.machineCollection.doc(key).update({'oneK.counter':'0'});
      }).then(()=>{
        if (this.data[i].twoFifty.status==true && this.data[i].sixK.status==true) 
        {
          this.machineCollection.doc(key).update({'allSerStatus':true});
        } 
      }).catch(err=>{this.msg.error('Connection Problem')});
      
    }

    sixK(key,i)
    {
      this.data=this.machineList;
      var previous=this.data[i].sixK.counter
      var d:boolean=true;
      this.machineCollection.doc(key).update({'sixK.lastStatus':previous}).then(()=>{
        this.machineCollection.doc(key).update({'sixK.status':d});
        this.machineCollection.doc(key).update({'sixK.counter':'0'});
      }).then(()=>{
        if (this.data[i].oneK.status==true && this.data[i].twoFifty.status==true) 
        {
          this.machineCollection.doc(key).update({'allSerStatus':true});
        } 
      }).catch(err=>{this.msg.error('Connection Problem')});
      
    }





    twoFifty2(key)
    {
      this.data222=this.machineList;
      for(let a of this.data222){
              if(a.$key==key)
              {
                this.machineCollection.doc(key).update({'twoFifty.lastStatus':a.twoFifty.counter}).then(()=>{
                  this.machineCollection.doc(key).update({'twoFifty.status':true});
                  this.machineCollection.doc(key).update({'twoFifty.counter':'0'});
                }).then(()=>{
                  if (a.oneK.status==true && a.sixK.status==true) 
                  {
                    this.machineCollection.doc(key).update({'allSerStatus':true});
                  } 
                }).catch(err=>{this.msg.error('Connection Problem')});
              }
      }
      
    }

    oneK2(key)
    {
      this.data222=this.machineList; 
      for(let a of this.data222){
              if(a.$key==key)
              {
                this.machineCollection.doc(key).update({'oneK.lastStatus':a.oneK.counter}).then(()=>{
                  this.machineCollection.doc(key).update({'oneK.status':true});
                  this.machineCollection.doc(key).update({'oneK.counter':'0'});
                }).then(()=>{
                  if (a.twoFifty.status==true && a.sixK.status==true) 
                  {
                    this.machineCollection.doc(key).update({'allSerStatus':true});
                  } 
                }).catch(err=>{this.msg.error('Connection Problem')});
              }
      }
    }

    sixK2(key)
    {
      this.data222=this.machineList;
      for(let a of this.data222){
        if(a.$key==key)
        {
          this.machineCollection.doc(key).update({'sixK.lastStatus':a.sixK.counter}).then(()=>{
            this.machineCollection.doc(key).update({'sixK.status':true});
            this.machineCollection.doc(key).update({'sixK.counter':'0'});
          }).then(()=>{
            if (a.oneK.status==true && a.twoFifty.status==true) 
            {
              this.machineCollection.doc(key).update({'allSerStatus':true});
            } 
          }).catch(err=>{this.msg.error('Connection Problem')});
        }
       }
       
    }

    lastFresh(i,k)
    {
       this.data=this.machineList;
       var c=this.machineList;
      var j=(parseInt(k)-parseInt(this.data[i].totalHours));
      var twoFifty1=parseInt(this.data[i].twoFifty.counter)+j;
      var oneK1=parseInt(this.data[i].oneK.counter)+j;
      var sixK1=parseInt(this.data[i].sixK.counter)+j;
      var check2=parseInt(this.data[i].twoFifty.check);
      var check1=parseInt(this.data[i].oneK.check);
      var check6=parseInt(this.data[i].sixK.check);
      this.test2=c[i].dailyHour[c[i].dailyHour.length-1];
      var d= this.test2.hours;
      var e=this.test2.date;
      if(d==k)
      {
          if(parseInt(i+1)==c.length)
          {
            setTimeout(() => {
              document.getElementById('cancel').click();
              this.msg.success('Updated');
            }, 1000);
          }
      }
      else
      {
      this.machineCollection.doc(c[i].$key).update({dailyHour:firebase.firestore.FieldValue.arrayRemove({date:e,hours:d})}).then(()=>{
        this.machineCollection.doc(this.data[i].$key).update({dailyHour:firebase.firestore.FieldValue.arrayUnion({date:e,hours:k})}).catch(err=>{this.msg.error('Connection Problem')});
      }).then(()=>{
        this.machineCollection.doc(this.data[i].$key).update({totalHours:k});
      }).catch(err=>{
        this.msg.error('Connection Problem');
      }).then(()=>{
      if(this.data[i].twoFifty.counter=='0') this.machineCollection.doc(this.data[i].$key).update({'twoFifty.counter':'0'});
          else this.machineCollection.doc(this.data[i].$key).update({'twoFifty.counter':twoFifty1+''});
          if(this.data[i].oneK.counter=='0') this.machineCollection.doc(this.data[i].$key).update({'oneK.counter':'0'});
          else this.machineCollection.doc(this.data[i].$key).update({'oneK.counter':oneK1+''});
          if(this.data[i].sixK.counter=='0') this.machineCollection.doc(this.data[i].$key).update({'sixK.counter':'0'});
          else this.machineCollection.doc(this.data[i].$key).update({'sixK.counter':sixK1+''});          
            }).then(()=>{
              if (twoFifty1>=check2) 
              {
                var d:boolean=false;
                this.machineCollection.doc(this.data[i].$key).update({'twoFifty.status':d});
              }
              else
              {
                this.machineCollection.doc(this.data[i].$key).update({'twoFifty.status':true});
              } 
              if (oneK1>=check1) 
              {
                var d1:boolean=false;
                this.machineCollection.doc(this.data[i].$key).update({'oneK.status':d1});
              }
              else
              {
                this.machineCollection.doc(this.data[i].$key).update({'oneK.status':true});
              } 
              if (sixK1>=check6) 
              {
                var d2:boolean=false;
                this.machineCollection.doc(this.data[i].$key).update({'sixK.status':d2});
              }
              else
              {
                this.machineCollection.doc(this.data[i].$key).update({'sixK.status':true});
              } 
            }).then(()=>{
              if (twoFifty1>=check2 || oneK1>=check1 || sixK1>=check6) 
              {
                 this.machineCollection.doc(this.data[i].$key).update({'allSerStatus':false});
              } 
              else 
              {
                this.machineCollection.doc(this.data[i].$key).update({'allSerStatus':true});
              }
            })
          if(parseInt(i+1)==c.length)
          {
            setTimeout(() => {
              document.getElementById('cancel').click();
              this.msg.success('Updated');
            }, 2000);
            
          }
        }  
    }


    lastEntered(i,j)
    {
      this.data=this.machineList;
      var c=this.machineList;
      this.test=c[i].dailyHour[c[i].dailyHour.length-2];
      this.test2=c[i].dailyHour[c[i].dailyHour.length-1];
      var d= this.test.hours;
      var e=this.test2.hours;
      var f=this.test.date;
      var g=this.test2.date;
      if(d==j)
      {
          if(parseInt(i+1)==c.length)
          {
            setTimeout(() => {
              document.getElementById('cancel').click();
              this.msg.success('Updated');
            }, 1000);
          }
      }
      else
      {
          this.machineCollection.doc(c[i].$key).update({dailyHour:firebase.firestore.FieldValue.arrayRemove({date:g,hours:e})}).then(()=>{
            this.machineCollection.doc(c[i].$key).update({dailyHour:firebase.firestore.FieldValue.arrayRemove({date:f,hours:d})});
          }).then(()=>{
            this.machineCollection.doc(this.data[i].$key).update({dailyHour:firebase.firestore.FieldValue.arrayUnion({date:f,hours:j})});
          }).then(()=>{
            this.machineCollection.doc(this.data[i].$key).update({dailyHour:firebase.firestore.FieldValue.arrayUnion({date:g,hours:e})});
          }).catch(err=>{
            this.msg.error('Connection Problem');
          }); 
          if(parseInt(i+1)==c.length)
          {
            setTimeout(() => {
              document.getElementById('cancel').click();
              this.msg.success('Updated');
            }, 2000);
            
          } 
      }
    }

    insuranceCheck()
    {
      this.data=this.machineList;
      for(let i=0;i<this.data.length;i++)
      {
          if(this.date_diff_indays(new Date(),new Date(this.data[i].insurance[this.data[i].insurance.length-1].expireDate.seconds*1000))<=30)
        {
          this.machineCollection.doc(this.data[i].$key).update({'insrncStatus':false});
        }
          else this.machineCollection.doc(this.data[i].$key).update({'insrncStatus':true});
      }
    }

    /////////////////////////////////////////POC/////////////////////////////////////////////////////

    updateArray(i,k,con:boolean)
    {
      this.data=this.machineList;
      var removeD=this.data[i].dailyHour[this.data[i].dailyHour.length-1].date;
      var removeH=this.data[i].dailyHour[this.data[i].dailyHour.length-1].hours;
      if(con)
      {
        if(parseInt(k)!=parseInt(removeH))
        {
          this.machineCollection.doc(this.data[i].$key).update({dailyHour:firebase.firestore.FieldValue.arrayRemove({date:removeD,hours:removeH})}).then(()=>{
            this.machineCollection.doc(this.data[i].$key).update({dailyHour:firebase.firestore.FieldValue.arrayUnion({date:(new Date()),hours:k})}).catch(err=>{this.msg.error("cant't Update Value",'Check Internet Connection')});
          }).catch(err=>{
            this.msg.error("can't update",'check connection')
          })
        }
      }
      else
      {
        this.machineCollection.doc(this.data[i].$key).update({dailyHour:firebase.firestore.FieldValue.arrayUnion({date:(new Date()),hours:k})}).catch(err=>{this.msg.error("cant't Update Value",'Check Internet Connection')});
      }
      if(i==this.data.length)
      {
        this.notifications();
      }

    }
    notifications() {
      var data1 = 'hii';
       console.log(data1);
       var date = new Date(this.data.date + " " + this.data.time);
       console.log(date);
       this.localNotifications.hasPermission().then(() => {
   
         this.localNotifications.schedule({
           text: 'Delayed ILocalNotification',
           led: 'FF0000',
           sound: this.setSound(),
         });
         data1 = '';
       }).catch((err) => {
         alert(err);
       });
   
   
     }
     setSound() {
       if (this.platform.is('android')) {
         return 'file://assets/sounds/Rooster.mp3'
       } else {
         return 'file://assets/sounds/Rooster.caf'
       }
     }
    updateTotal(i,k)
    {
      this.data=this.machineList;
      var total=parseInt(this.data[i].totalHours);
      var recived=parseInt(k);
      if(total==recived){}
      else this.machineCollection.doc(this.data[i].$key).update({totalHours:k+''}).catch(err=>{
        this.msg.error('Connection Problem')
      });
    }

    updateTwoC(i,k:number)
    {
      this.data=this.machineList;
      var previous=parseInt(this.data[i].twoFifty.counter);
      var check=parseInt(this.data[i].twoFifty.check);
      var n=previous+k;
      if(k==0){}
      else
      {
        this.machineCollection.doc(this.data[i].$key).update({'twoFifty.counter':n+''}).then(()=>{
          if(n>check)
          {
            this.machineCollection.doc(this.data[i].$key).update({'twoFifty.status':false});
            this.machineCollection.doc(this.data[i].$key).update({'allSerStatus':false});
          }
        }).catch(err=>{
          this.msg.error('Connection Problem')
        });
      }
    }

    updateOneC(i,k:number)
    {
      this.data=this.machineList;
      var previous=parseInt(this.data[i].oneK.counter);
      var check=parseInt(this.data[i].oneK.check);
      var n=previous+k;
      if(k==0){}
      else
      {
        this.machineCollection.doc(this.data[i].$key).update({'oneK.counter':n+''}).then(()=>{
          if(n>check)
          {
            this.machineCollection.doc(this.data[i].$key).update({'oneK.status':false});
            this.machineCollection.doc(this.data[i].$key).update({'allSerStatus':false});
          }
        }).catch(err=>{
          this.msg.error('Connection Problem')
        });
      }
    }

    updateSixC(i,k:number)
    {
      this.data=this.machineList;
      var previous=parseInt(this.data[i].sixK.counter);
      var check=parseInt(this.data[i].sixK.check);
      var n=previous+k;
      if(k==0){}
      else
      {
        this.machineCollection.doc(this.data[i].$key).update({'sixK.counter':n+''}).then(()=>{
          if(n>check)
          {
            this.machineCollection.doc(this.data[i].$key).update({'sixK.status':false});
            this.machineCollection.doc(this.data[i].$key).update({'allSerStatus':false});
          }
        }).catch(err=>{
          this.msg.error('Connection Problem')
        });
      }
    }

    updateInsurance(key,issue:Date,expire:Date,amount)
    {
      this.machineCollection.doc(key).update({insurance:firebase.firestore.FieldValue.arrayUnion({amount:amount,issueDate:issue,expireDate:expire})}).then(()=>{
        this.machineCollection.doc(key).update({'insrncStatus':true});
      }).then(()=>
      {
        this.msg.success('Done');
        document.getElementById('cancel').click();
      }).catch(()=>{
        this.msg.error('Connection Problem')
      });
    }


  }

