import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";
import { CanActivate, Router } from '@angular/router';
import { UserService } from "../users/user.service";
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate
{
    constructor(private auth:UserService,private router:Router) { }

    canActivate()
    {
        return this.auth.user$.pipe(map(user => {
          if(user) return true;

          this.router.navigateByUrl('/login');
          return false;
        }));
    }
}